﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

public class CharacterReaction : MonoBehaviour {

    
    public Animator animator;
    public ParticleSystem ps;

    private GazeAware _gazeAware;
    private float timeSinceStartWatching = 0f;

    void Start()
    {
        _gazeAware = GetComponent<GazeAware>();
    }

    void Update()
    {
        if (_gazeAware.HasGazeFocus)
        {
            animator.SetBool("param_watching", true);
            if (timeSinceStartWatching == 0)
            {
                timeSinceStartWatching = Time.time;
            }
        }
        else
        {
            animator.SetBool("param_watching", false);
            timeSinceStartWatching = 0;
        }

        if (Time.time - timeSinceStartWatching > 3f && timeSinceStartWatching != 0)
        {
            if (!ps.isPlaying)
            {
                ps.Play();
            }
        }
        else
        {
            if (ps.isPlaying)
            {
                ps.Stop();
            }
        }
    }
}
