﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

public class GazeControl : MonoBehaviour {

    [Range(3.0f, 100.0f)]
    public float visualizationDistance = 10f;
    public GameObject tobiiIndicator;
    public GameObject flashLight;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Test();
        RotateFlashLightToGaze();
    }

    private void Test()
    {
        if (tobiiIndicator != null)
        {
            Vector2 filteredPoint = Vector2.zero;

            GazePoint gaze = TobiiAPI.GetGazePoint();

            Vector3 objectPosition = ProjectToPlaneInWorld(gaze);


            tobiiIndicator.transform.position = objectPosition;


            // Vector2 gazePoint = TobiiAPI.GetGazePoint().Screen;
            // filteredPoint = Vector3.Lerp(filteredPoint, objectPosition, 0.5f);
            // tobiiIndicator.transform.position = filteredPoint;
        }
    }

    private void RotateFlashLightToGaze()
    {
        GazePoint gaze = TobiiAPI.GetGazePoint();
        if (gaze.IsRecent())
        {
            var vector = ProjectToPlaneInWorld(gaze);
            flashLight.transform.LookAt(vector);
        }
        
    }

    private Vector3 ProjectToPlaneInWorld(GazePoint gazePoint)
    {
        if (gazePoint.IsRecent())
        {
            Vector3 gazeOnScreen = gazePoint.Screen;
            gazeOnScreen += (transform.forward * visualizationDistance);
            return Camera.main.ScreenToWorldPoint(gazeOnScreen);
        }
        return Vector3.zero;
    }
}
