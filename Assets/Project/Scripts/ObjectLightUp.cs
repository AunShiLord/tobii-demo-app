﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

public class ObjectLightUp : MonoBehaviour {

    public Light spotlight;
    public AnimationCurve animCurve;
    public float lightUpTime = 1f;

    private GazeAware _gazeAware;
    private float timeSinceStart = 0f;
    private bool isWatched = false;

    // Use this for initialization
    void Start () {
        _gazeAware = GetComponent<GazeAware>();

    }

    // Update is called once per frame
    void Update()
    {
        if (_gazeAware.HasGazeFocus)
        {
            spotlight.intensity = 3f;
        }
        else
        {
            spotlight.intensity = 0;
        }
    }
}
